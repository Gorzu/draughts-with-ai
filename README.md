# README #

Graficzna gra w warcaby napisana w C++ z użyciem biblioteki SFML - człowiek kontra komputer.
Kod programu zawiera algorytm sztucznej inteligencji w oparciu o strategię Minimax i funkcję heurystyczną.

### Zasady gry ###

* Człowiek zawsze wybiera stronę, po której chce grać.
* Bicie jest obowiązkowe.
* Jeśli po zbiciu możliwe jest wykonanie kolejnego bicia, to również jest ono obowiązkowe.
* Zwykłe piony mogą poruszać się tylko do przodu. Wyjątkiem jest bicie, które można wykonywać także do tyłu.
* Jeśli pion dotrze do pierwszego rzędu przeciwnika, otrzymuje promocję.
* Remis następuje, gdy przez 14 ruchów (7 pełnych tur) pionami królewskimi nie zostanie zbity żaden pion.

### Do kogo należy gra? ###

Właściciel projektu: Piotr Gorzelnik.

